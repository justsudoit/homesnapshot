# Edit the following variables to suit your needs
# A guide to configuring this file can be found here:
# https://gitlab.com/justsudoit/homesnapshot/-/wikis/Configuration
# When you are done editing, you can test your configuration by 
# running "homesnapshot -t"
#
# The number of snapshots to keep
# This number should be an integer greater than or equal to 1

KEEP=4

# DEST is short for destination, it is the location to store your snapshots 
# Use absolute path unless the location is on a different filesystem such as a seperate
# storage device or on a remote host. The filesystem in this location MUST support
# linux filesystem permissions, such as ext4, xfs, zfs, btrfs. NTFS and FAT will NOT work.
# Escape spaces with "\", the scripted configurator can do this automatically
# This should be somewhere outside your home directory that you have read & write permission
# Do NOT leave a trailing "/", the scripted configurator will drop a trailing "/"
# Note, if the destination already has directories that begin with your username and colon,
# such as "alex:" if your username is alex, this script will treat them like they are a snapshot

DEST=/path/to\ your/snapshots

# Select your package manager if you wish to export your list of explicitly installed packages
# If you package manager is not shown, leave these commented out
# Using one pacakage manager to install a list created by another probably won't work, or at least
# not completely. It would not hurt to try though, because packages not found will be ignored.

#PKG=apt
#PKG=pacman
#PKG=dnf

# This is the list of files and folders to exclude from your snapshots
# All items should be relative to your home directory, one entry per line, 
# keep them in between the parenthesis. You can add as many or as few entries as you like. 
# The entries do not need to exist in your home directory, ie: you can leave "snap" 
# here even if you do not have a "snap" folder
# These are given as examples

EXCLUDE=(
Downloads
snap
.cache
.thunderbird
.local/share/Trash
.local/share/Steam/steamapps/common
)

# Valid options for REMOTE are yes and no
# Set this value to yes if you are sending snapshots to a remote machine over ssh
# Note; if this is set to yes, the destination set above will be used as the 
# destination path on the remote host.

REMOTE=no

# If REMOTE=yes then fill out the rest of these values accordingly
# REMOTEHOST can be an ip address or a hostname from your ssh config

REMOTEHOST=hostname
REMOTEUSER=user

# You will also need to create a passwordless ssh key.
# A guide on how to do this can be found here: 
# https://gitlab.com/justsudoit/homesnapshot/-/wikis/Set-up-SSH
#
# If your output appears strange, or if you are running non-interactively
# then uncomment COLOR=0 to turn off special formatting
# Alternate colors can be accessed using COLOR=2 and COLOR=3, default is COLOR=1

#COLOR=0

# Uncomment to turn on more verbose output. Useful for troubleshooting.

#DEBUG=yes 
